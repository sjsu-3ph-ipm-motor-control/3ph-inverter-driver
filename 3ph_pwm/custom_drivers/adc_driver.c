
#include "adc_driver.h"

bool adc_driver__init(adc_driver_s* phaseU, adc_driver_s* phaseV, adc_driver_s* phaseW) {
    //Initialize objects
    phaseU->phase = PHASE_U;
    phaseU->raw_voltage = 0;
    phaseU->raw_current = 0;

    phaseV->phase = PHASE_V;
    phaseV->raw_voltage = 0;
    phaseV->raw_current = 0;

    phaseW->phase = PHASE_W;
    phaseW->raw_voltage = 0;
    phaseW->raw_current = 0;

    // Set ADCCLK divider to /4
    ADC_setPrescaler(ADCA_BASE, ADC_CLK_DIV_4_0);
    ADC_setPrescaler(ADCC_BASE, ADC_CLK_DIV_4_0);

    // Set resolution and signal mode (see #defines above) and load
    // corresponding trims.
    ADC_setMode(ADCA_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);
    ADC_setMode(ADCC_BASE, ADC_RESOLUTION_12BIT, ADC_MODE_SINGLE_ENDED);

    // Set pulse positions to late
    ADC_setInterruptPulseMode(ADCA_BASE, ADC_PULSE_END_OF_CONV);
    ADC_setInterruptPulseMode(ADCC_BASE, ADC_PULSE_END_OF_CONV);

    // Power up the ADCs and then delay for 1 ms
    ADC_enableConverter(ADCA_BASE);
    ADC_enableConverter(ADCC_BASE);

    DEVICE_DELAY_US(1000);

    // - SOC0 will convert pin A0.
    // - SOC1 will convert pin A1.

    //Phase U voltage - ADC A SOC0
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER0, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN0, 15);
    //Phase V voltage - ADC A SOC2
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 15);
    //Phase W voltage - ADC A SOC4
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER4, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN4, 15);

    //Phase U current - ADC A SOC14
    ADC_setupSOC(ADCA_BASE, ADC_SOC_NUMBER14, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN14, 15);
    //Phase U current - ADC C SOC2
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER2, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN2, 15);
    //Phase U current - ADC C SOC4
    ADC_setupSOC(ADCC_BASE, ADC_SOC_NUMBER4, ADC_TRIGGER_SW_ONLY,
                 ADC_CH_ADCIN4, 15);

    return true;
}

void adc_driver__start_phase_conversion(adc_driver_s* adc_object) {
    switch (adc_object->phase) {
    case PHASE_U:
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER0);
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER14);
    case PHASE_V:
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER2);
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER2);
    case PHASE_W:
        ADC_forceSOC(ADCA_BASE, ADC_SOC_NUMBER4);
        ADC_forceSOC(ADCC_BASE, ADC_SOC_NUMBER4);
    }
}

void adc_driver__read_phase_values(adc_driver_s* adc_object) {
    switch (adc_object->phase) {
    case PHASE_U:
        adc_object->raw_voltage = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER0);
        adc_object->raw_current = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER14);
    case PHASE_V:
        adc_object->raw_voltage = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER2);
        adc_object->raw_current = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER2);
    case PHASE_W:
        adc_object->raw_voltage = ADC_readResult(ADCARESULT_BASE, ADC_SOC_NUMBER4);
        adc_object->raw_current = ADC_readResult(ADCCRESULT_BASE, ADC_SOC_NUMBER4);
    }
}

//Not functioning
//void adc_driver__register_interrupt_handler(uint32_t adc_base, void (*handler)(void)){
//    // Assign the interrupt service routine to ePWM interrupt
//    Interrupt_register(INT_ADCA1, handler);
//}
//
//void adc_driver__enable_interrupt(adc_driver_s* adc_object){
//    // Set SOC1 to set the interrupt 1 flag. Enable the interrupt and make
//    // sure its flag is cleared.
//    ADC_setInterruptSource(ADCA_BASE, ADC_INT_NUMBER1, ADC_SOC_NUMBER1);
//    ADC_enableInterrupt(ADCA_BASE, ADC_INT_NUMBER1);
//    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
//}

