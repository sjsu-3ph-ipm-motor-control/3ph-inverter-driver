/* This module simplifies the setup of ePWM peripherals
 * Only functional for epwm 1 - 8
 */



#ifndef PWM_DRIVER_H_
#define PWM_DRIVER_H_

#include <stdint.h>

#include "epwm.h"
#include "device.h"

//
// Defines
//
#define EPWM1_TIMER_TBPRD  2000U
#define EPWM1_MAX_CMPA     1000U
#define EPWM1_MIN_CMPA     1000U
#define EPWM1_MAX_CMPB     1000U
#define EPWM1_MIN_CMPB     1000U

typedef struct
{
    uint16_t pwm_number;
    uint32_t epwmModule;
    uint32_t pwm_freq_hz;
    uint32_t tbprd;
    uint16_t gpio_pin_A_num;
    uint16_t gpio_pin_B_num;
    uint32_t gpio_pin_A_config;
    uint32_t gpio_pin_B_config;
    float duty_cycle_percent;
    uint32_t compare_A_value;
}pwm_driver_s;



bool pwm_driver__init(pwm_driver_s* pwm_object,
                      uint16_t epwm_number,
                      uint32_t frequency_hz,
                      float duty_cycle_A_percent);

void pwm_driver__set_duty_cycle(pwm_driver_s* pwm_object, float percent);

void pwm_driver__set_frequency(pwm_driver_s* pwm_object, uint16_t frequency_hz);

void pwm_driver__configure_deadtime(pwm_driver_s* pwm_object);

void pwm_driver__register_interrupt_handler(uint32_t epwm_number, void (*handler)(void));

void pwm_driver__enable_interrupt(pwm_driver_s* pwm_object);

#endif /* PWM_DRIVER_H_ */
