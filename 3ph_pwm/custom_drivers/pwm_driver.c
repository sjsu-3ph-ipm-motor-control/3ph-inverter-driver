#include "pwm_driver.h"



bool pwm_driver__init(pwm_driver_s* pwm_object,
                      uint16_t epwm_number,
                      uint32_t frequency_hz,
                      float duty_cycle_A_percent) {

    // Store all relevant values in the driver struct
    pwm_object->pwm_number = epwm_number;
    pwm_object->epwmModule = EPWM1_BASE + 0x100U * (epwm_number - 1);
    pwm_driver__set_frequency(pwm_object, frequency_hz);
    pwm_object->gpio_pin_A_num = 2 * epwm_number - 2;
    pwm_object->gpio_pin_B_num = 2 * epwm_number - 1;
    pwm_object->gpio_pin_A_config = GPIO_0_EPWM1A + 0x400U * (epwm_number - 1);
    pwm_object->gpio_pin_B_config = pwm_object->gpio_pin_A_config + 0x200U;
    pwm_driver__set_duty_cycle(pwm_object, duty_cycle_A_percent);


    GPIO_setPadConfig(pwm_object->gpio_pin_A_num, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(pwm_object->gpio_pin_A_config);
    GPIO_setPadConfig(pwm_object->gpio_pin_B_num, GPIO_PIN_TYPE_STD);
    GPIO_setPinConfig(pwm_object->gpio_pin_B_config);

    // Set up counter mode
    EPWM_setTimeBaseCounterMode(pwm_object->epwmModule, EPWM_COUNTER_MODE_UP_DOWN);
    EPWM_disablePhaseShiftLoad(pwm_object->epwmModule);
    EPWM_setClockPrescaler(pwm_object->epwmModule,
                           EPWM_CLOCK_DIVIDER_1,
                           EPWM_HSCLOCK_DIVIDER_1);

    // Set actions
    EPWM_setActionQualifierAction(pwm_object->epwmModule,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(pwm_object->epwmModule,
                                  EPWM_AQ_OUTPUT_A,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);
    EPWM_setActionQualifierAction(pwm_object->epwmModule,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_HIGH,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA);
    EPWM_setActionQualifierAction(pwm_object->epwmModule,
                                  EPWM_AQ_OUTPUT_B,
                                  EPWM_AQ_OUTPUT_LOW,
                                  EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA);

    return true;
}

void pwm_driver__set_duty_cycle(pwm_driver_s* pwm_object, float percent){
    pwm_object->duty_cycle_percent = percent;
    pwm_object->compare_A_value = (uint32_t) (pwm_object->tbprd * percent / 100.0);

    // Set Compare value
    EPWM_setCounterCompareValue(pwm_object->epwmModule,
                                EPWM_COUNTER_COMPARE_A,
                                pwm_object->compare_A_value);
}

void pwm_driver__set_frequency(pwm_driver_s* pwm_object, uint16_t frequency_hz){
    pwm_object->pwm_freq_hz = frequency_hz;
    pwm_object->tbprd = 100000000 / (2 * frequency_hz); // EPWM Clock speed is 100MHz, and no prescaling on epwm1


    // Set-up TBCLK
    EPWM_setTimeBasePeriod(pwm_object->epwmModule, pwm_object->tbprd);
    EPWM_setPhaseShift(pwm_object->epwmModule, 0U);
    EPWM_setTimeBaseCounter(pwm_object->epwmModule, 0U);

    pwm_object->compare_A_value = (uint32_t) (pwm_object->tbprd * pwm_object->duty_cycle_percent / 100.0);
//    pwm_object->compare_B_value = (uint32_t) (pwm_object->tbprd * pwm_object->duty_cycle_B_percent / 100.0);

    // Set Compare value
    EPWM_setCounterCompareValue(pwm_object->epwmModule,
                                EPWM_COUNTER_COMPARE_A,
                                pwm_object->compare_A_value);
}

void pwm_driver__configure_deadtime(pwm_driver_s* pwm_object){

}

void pwm_driver__register_interrupt_handler(uint32_t epwm_number, void (*handler)(void)){
    // Assign the interrupt service routine to ePWM interrupt
    Interrupt_register(INT_EPWM1 + (0x10001U * (epwm_number - 1)), handler);
}

void pwm_driver__enable_interrupt(pwm_driver_s* pwm_object){
    // Interrupt where we will change the Compare Values
    // Select INT on Time base counter zero event,
    // Enable INT, generate INT on 3rd event
    EPWM_setInterruptSource(pwm_object->epwmModule, EPWM_INT_TBCTR_ZERO);
    EPWM_enableInterrupt(pwm_object->epwmModule);
    EPWM_setInterruptEventCount(pwm_object->epwmModule, 1U);

    // Enable ePWM interrupt
    Interrupt_enable(INT_EPWM1 + (0x10001U * (pwm_object->pwm_number - 1)));
}

