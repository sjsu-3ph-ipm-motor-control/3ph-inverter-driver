

#ifndef ADC_DRIVER_H_
#define ADC_DRIVER_H_

#include <stdint.h>

#include "adc.h"
#include "device.h"

//
// Defines
//
typedef enum {
    PHASE_U,
    PHASE_V,
    PHASE_W,
}adc_phase;

typedef struct {
    adc_phase phase;
    uint16_t raw_voltage;
    uint16_t raw_current;
}adc_driver_s;

bool adc_driver__init(adc_driver_s* phaseU, adc_driver_s* phaseV, adc_driver_s* phaseW);

void adc_driver__start_phase_conversion(adc_driver_s* adc_object);

void adc_driver__read_phase_values(adc_driver_s* adc_object);

//void adc_driver__register_interrupt_handler(uint32_t adc_base, void (*handler)(void));
//
//void adc_driver__enable_interrupt(adc_driver_s* adc_object);

#endif /* ADC_DRIVER_H_ */
