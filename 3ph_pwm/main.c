
//
// Includes
//
#include <math.h>

#include "driverlib.h"
#include "device.h"
#include "pwm_driver.h"
#include "adc_driver.h"

//
// Defines
//
#define PWM_BASE_FREQUENCY 20000


//
// Globals
//
volatile uint32_t counter = 0;
volatile uint16_t adc_value = 0;

//epwmInformation epwm1Info;
pwm_driver_s pwmU;
pwm_driver_s pwmV;
pwm_driver_s pwmW;
pwm_driver_s pwm_test;
adc_driver_s adc_phaseU;
adc_driver_s adc_phaseV;
adc_driver_s adc_phaseW;



//
// Function prototypes
//
__interrupt void epwm1_isr();
//__interrupt void adc_a1_isr();

void main(void)
{
    Device_init();
    Device_initGPIO();


    Interrupt_initModule();
    Interrupt_initVectorTable();

    // Register interrupt handler
    pwm_driver__register_interrupt_handler(1, &epwm1_isr);
//    adc_driver__register_interrupt_handler(0, &adc_a1_isr);

    SysCtl_disablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    pwm_driver__init(&pwmU, 1, PWM_BASE_FREQUENCY, 50);
    pwm_driver__init(&pwmV, 2, PWM_BASE_FREQUENCY, 50);
    pwm_driver__init(&pwmW, 5, PWM_BASE_FREQUENCY, 50);
    pwm_driver__init(&pwm_test, 6, PWM_BASE_FREQUENCY, 50);

    SysCtl_enablePeripheral(SYSCTL_PERIPH_CLK_TBCLKSYNC);

    pwm_driver__enable_interrupt(&pwmU);

    adc_driver__init(&adc_phaseU, &adc_phaseV, &adc_phaseW);

    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    EINT;
    ERTM;


    for(;;){
        NOP;
    }

}


__interrupt void epwm1_isr(){
    if (0 == counter % 10){
        double valueU = 50 * sin((double)counter * 2 * 3.14 / 1000) + 50;
        uint32_t counterV = counter + 333 > 1000 ? counter - 667 : counter + 333;
        double valueV = 50 * sin((double)counterV * 2 * 3.14 / 1000) + 50;
        uint32_t counterW = counter + 667 > 1000 ? counter - 333 : counter + 667;
        double valueW = 50 * sin((double)counterW * 2 * 3.14 / 1000) + 50;

        pwm_driver__set_duty_cycle(&pwmU, valueU);
        pwm_driver__set_duty_cycle(&pwmV, valueV);
        pwm_driver__set_duty_cycle(&pwmW, valueW);

    }

    counter = counter >= 1000 ? 0 : counter + 1;

//    adc_driver__read_phase_values(&adc_phaseU);
//    pwm_driver__set_duty_cycle(&pwmU, (float) adc_value * 100 / 0xFFF);
//
//    adc_driver__start_phase_conversion(&adc_phaseU);

    // Clear INT flag for this timer
    EPWM_clearEventTriggerInterruptFlag(EPWM1_BASE);

    // Acknowledge interrupt group
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP3);
}

//__interrupt void adc_a1_isr(){
//    adc_driver__read_phase_values(&adc_phaseU);
//
//    ADC_clearInterruptStatus(ADCA_BASE, ADC_INT_NUMBER1);
//
//    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP1);
//}

